#ifndef MPRISBASE_H
#define MPRISBASE_H

#include <QObject>
#include <QDBusAbstractAdaptor>

class MprisBase : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.mpris.MediaPlayer2")

    Q_PROPERTY(bool CanSetFullscreen READ CanSetFullscreen)
    Q_PROPERTY(bool CanQuit READ CanQuit)
    Q_PROPERTY(bool CanRaise READ CanRaise)
    Q_PROPERTY(bool HasTrackList READ HasTrackList)
    Q_PROPERTY(QString Identity READ Identity)
    Q_PROPERTY(QString DesktopEntry READ DesktopEntry)
    Q_PROPERTY(QStringList SupportedUriSchemes READ SupportedUriSchemes)
    Q_PROPERTY(QStringList SupportedMimeTypes READ SupportedMimeTypes)
    Q_PROPERTY(bool FullScreen READ FullScreen WRITE setFullScreen)

public:
    explicit MprisBase(QObject *parent = 0);

    bool FullScreen() const { return false; }
    bool CanSetFullscreen() const { return false; }
    bool CanQuit() const { return false; }
    bool CanRaise() const { return false; }
    bool HasTrackList() const { return false; }

    QStringList SupportedUriSchemes();
    QStringList SupportedMimeTypes();
    QString Identity();
    QString DesktopEntry();
    void setFullScreen(const bool);

private:
    QString m_dekstopEntry;
    QString m_identity;

public slots:
    void Raise();
    void Quit();
};

/*
https://specifications.freedesktop.org/mpris-spec/latest/Media_Player.html
*/

#endif // MPRISBASE_H
