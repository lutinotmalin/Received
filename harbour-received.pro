# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-received

QT += multimedia dbus

CONFIG += sailfishapp
CONFIG += sailfishapp_i18n

HEADERS += \
    src/settings.h \
    src/audioplayer.h \
    src/mprisbase.h \
    src/mprisplayer.h

SOURCES += \
    src/harbour-received.cpp \
    src/settings.cpp \
    src/audioplayer.cpp \
    src/mprisbase.cpp \
    src/mprisplayer.cpp

DISTFILES += \
    qml/components/audioplayer/DockedAudioPlayerForm.ui.qml \
    qml/components/audioplayer/DockedAudioPlayer.qml \
    qml/components/audioplayer/PlayerControlsForm.ui.qml \
    qml/components/contextmenus/FavoritesListContextMenu.qml \
    qml/components/contextmenus/StationsListContextMenu.qml \
    qml/components/CustomSearchField.qml \
    qml/components/MenuLabelSmal.ui.qml \
    qml/components/NavigationMenuForm.ui.qml \
    qml/components/NavigationMenu.qml \
    qml/components/RadioAPI.qml \
    qml/components/RecivedUpgrader.qml \
    qml/components/StationDelegate.qml \
    qml/cover/CoverPage.qml \
    qml/pages/BrowsePageForm.ui.qml \
    qml/pages/BrowsePage.qml \
    qml/pages/BrowseByCategoryPageForm.ui.qml \
    qml/pages/BrowseByCategoryPage.qml \
    qml/pages/FavoritesPageForm.ui.qml \
    qml/pages/FavoritesPage.qml \
    qml/pages/FavoritePageForm.ui.qml \
    qml/pages/FavoritePage.qml \
    qml/pages/SettingsPageForm.ui.qml \
    qml/pages/SettingsPage.qml \
    qml/pages/StationsPageForm.ui.qml \
    qml/pages/StationsPage.qml \
    qml/harbour-received.qml \
    qml/cover/layouts/CoverDesign.qml \
    qml/components/audioplayer/StationInfoLabel.qml \
    qml/components/listmodels/ApiLanguageListModel.qml \
    qml/components/audioplayer/SleepTimerSwitch.qml \
    qml/components/sleeptimer/SleepTimerDialogForm.ui.qml \
    qml/components/sleeptimer/SleepTimerDialog.qml \
    qml/components/sleeptimer/SleepTimer.qml \
    qml/components/listmodels/PlayerLayoutListModel.qml \
    qml/components/audioplayer/DockedAudioPlayerLoader.qml \
    qml/components/audioplayer/DockedAudioPlayerSmall.qml \
    qml/components/audioplayer/DockedAudioPlayerSmallForm.ui.qml

OTHER_FILES += \
    rpm/harbour-received.changes.in \
    rpm/harbour-received.changes.run.in \
    rpm/harbour-received.spec \
    rpm/harbour-received.yaml \
    translations/*.ts \
    qml/components/js/utils/log.js \
    qml/components/js/Utils.js \
    qml/components/js/Storage.js \
    qml/components/js/Favorites.js \
    qml/components/js/Rad.js \
    qml/components/python/api.py \
    harbour-received.desktop

TRANSLATIONS += translations/harbour-received-sv.ts \
                translations/harbour-received-es.ts

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

