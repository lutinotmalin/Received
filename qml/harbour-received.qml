import QtQuick 2.6
import org.nemomobile.notifications 1.0
import Sailfish.Silica 1.0
import it.kempe.AudioPlayer 1.0
import "pages"
import "components"
import "components/audioplayer" as Audio
import "./components/js/Storage.js" as DB
import "./components/js/Utils.js" as Utils

ApplicationWindow
{
    id: window

    property bool loading
    property var stationData
    property bool isPlaying: AudioPlayer.isPlaying

    allowedOrientations: Orientation.All
    bottomMargin: player.visibleSize
    initialPage: favorites
    cover: Qt.resolvedUrl("cover/CoverPage.qml")


    RecivedUpgrader {
    }

    FavoritesPage {
        id: favorites
    }

    Audio.DockedAudioPlayerLoader {
        id: player
    }

    BusyIndicator {
        anchors.centerIn: parent
        running: loading
        z: 1
    }

    RadioAPI {
        id: radioAPI
    }

    function playerLayoutUpdated() {
        player.updateDockedAudioPlayerSource();
    }
}


