import QtQuick 2.6
import "js/Storage.js" as DB

/**
 * Upgrader used to make necessary updates for new versions
 *
 * This upgrader is async and might need to be reimplemented for future upgrades
 * That is version 2 might be applied before version 1 is completed and so on
 */

Item {
    property int version: 1
    property int latestInstalledVersion: settings.value("latestInstalledVersion", 0)

    function updateFavoritesV1() {
        console.log("Updating favorites to version1")
        var stations = DB.loadStations();
        for(var i = 0; i < stations.rows.length; i++) {
            var row = stations.rows.item(i);
            radioAPI.updateFavorite(row);
        }
    }

    Component.onCompleted: {
        switch (latestInstalledVersion) {
            // We want fall-through to apply all updates from the lastestInstalledVersion

            case 0:
                updateFavoritesV1()
                settings.setValue("latestInstalledVersion", 1)
            case version:
                console.log("We are up to date")
        }
    }
}
