import QtQuick 2.6
import Sailfish.Silica 1.0
import "../js/Storage.js" as DB

SleepTimerDialogForm {
    id: root

    signal abort
    signal activateTimer(int sec)

    onAccepted: {
        DB.updateSleepTimer(hour, minute)
        var sec = (minute + (hour * 60)) * 60;
        activateTimer(sec)
    }

    onRejected: abort()

    Component.onCompleted: {
        var sTime = DB.loadSleepTimer()
        hour = sTime.hour
        minute = sTime.minute
        hourMode = DateTime.TwentyFourHours
    }
}
