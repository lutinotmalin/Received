import QtQuick 2.6
import Sailfish.Silica 1.0

ListItem {
    property string stationIcon: ""
    property string stationTitle: ""
    property string stationInfo: ""
    property string stationCurrent: ""
    property bool isPlaying: false

    contentHeight: Theme.itemSizeLarge

    anchors {
         left: parent.left
         leftMargin: Theme.horizontalPageMargin
         right: parent.right
         rightMargin: Theme.horizontalPageMargin
     }

    Rectangle {
        color: isPlaying ? Theme.primaryColor : "transparent"
        opacity: 0.1
        anchors.fill: parent
    }

    Row {
        spacing: Theme.paddingLarge

        Image {
            id: image
            source: stationIcon

            smooth: true
            fillMode: Image.PreserveAspectFit
            cache: true

            height: Theme.iconSizeLarge
            width: Theme.iconSizeLarge
            sourceSize.height: Theme.iconSizeLarge
            sourceSize.width: Theme.iconSizeLarge
        }

        Column {
            width: parent.width - image.width - Theme.paddingLarge
            height: parent.height
            spacing: -Theme.paddingSmall

            Label {
                text: stationTitle
            }

            Label {
                width: parent.width
                font.pixelSize: Theme.fontSizeExtraSmall
                truncationMode: TruncationMode.Fade
                color: Theme.secondaryColor
                text: stationInfo
                visible: stationInfo
            }

            Label {
                width: parent.width
                font.pixelSize: Theme.fontSizeSmall
                truncationMode: TruncationMode.Fade
                color: Theme.secondaryHighlightColor
                text: stationCurrent
                visible: stationCurrent
            }
        }
    }
}
