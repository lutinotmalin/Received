.pragma library
.import "Storage.js" as DB

var favoritesModel;

function init(model) {
    favoritesModel = model
    updateFavorites()
}

function updateFavorites() {
    if(favoritesModel)
        favoritesModel.clear();

    var stations = DB.loadStations();
    for(var i = 0; i < stations.rows.length; i++) {
        var row = stations.rows.item(i);
        favoritesModel.append(row);
    }
}

function toogleFavorite(station) {
    if(isFavorite(station))
        removeFavorite(station)
    else
        addFavorite(station)

    return isFavorite(station)
}

function removeFavorite(station) {
    console.log("Removing favorite:", JSON.stringify(station))
    DB.deleteStation(station)
    favoritesModel.remove(getModelIndexFromStation(station))
}

function addFavorite(station) {
    console.log("Saving favorite:", JSON.stringify(station))
    DB.saveStation(station)
    updateFavorites()
}

function getNextFavorite(currentStation) {
    if(!currentStation)
        return favoritesModel.get(0);

    var currentIndex = getModelIndexFromStation(currentStation);
    console.debug("Get next station. Current index:", JSON.stringify(currentIndex))

    var nextIndex = currentIndex+1
    if(nextIndex >= favoritesModel.count)
        return favoritesModel.get(0);

    return favoritesModel.get(nextIndex);
}

function getPrevFavorite(currentStation) {
    if(!currentStation)
        return favoritesModel.get(0);

    var currentIndex = getModelIndexFromStation(currentStation);
    console.log("currentIndex", currentIndex)

    var prevIndex = currentIndex-1
    if(prevIndex < 0)
        return favoritesModel.get(favoritesModel.count-1);

    return favoritesModel.get(prevIndex);
}

function isFavorite(station) {
    return DB.stationExists(station)
}

function isRadIoFavorite(radioId) {
    return DB.stationExistsByRadIoId(radioId)
}

function getFavoriteByRadioId(radIoId) {
    return DB.findByRadIoId(radIoId)
}

function getModelIndexFromStation(station) {
    console.log("Searching for id:", station.url)
    for (var i=0; i<favoritesModel.count; i++) {
        var s = favoritesModel.get(i);
        console.log("looking at url:", s.url)
        if(s.url === station.url)
            return i;
    }
}
