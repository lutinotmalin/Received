import QtQuick 2.6
import Sailfish.Silica 1.0
import "../"

Item {
    id: player

    property bool isPlaying
    property bool isFavorite
    property string currentTrack
    property int bufferProgress
    property bool showProgressBar
    property bool sleepTimerActive
    property string sleepTimerText

    property alias opener: opener
    property alias buttonPlay: controls.buttonPlay
    property alias buttonStar: controls.buttonStar
    property alias buttonPrevious: controls.buttonPrevious
    property alias buttonNext: controls.buttonNext
    property alias playerMenu: playerMenu
    property alias cancelSleepTimer: cancelSleepTimer
    property alias setSleepTimer: setSleepTimer


    property real hightPadding: Theme.paddingMedium
    height: Theme.itemSizeExtraLarge + (hightPadding * 2)

    MouseArea {
        id: opener
        anchors.fill: parent
    }

    Row {
        id: quickControlsItem
        anchors.fill: parent
        spacing: Theme.paddingLarge
        anchors.leftMargin: Theme.horizontalPageMargin
        anchors.rightMargin: Theme.horizontalPageMargin
        anchors.topMargin: hightPadding
        anchors.bottomMargin: hightPadding

        Image {
            id: stationIcon
            height: parent.height
            width: parent.height

            source: window.stationData ? window.stationData.stationLogo : ""
            smooth: true
            cache: true

            fillMode: Image.PreserveAspectFit
            sourceSize.height: height
            sourceSize.width: height
        }

        Column {
            id: trackInfo
            spacing: Theme.paddingSmall
            width: parent.width - stationIcon.width
            height: parent.height

            StationInfoLabel {
                name: window.stationData ? window.stationData.name : ""
                track: currentTrack
                width: parent.width
                height: parent.height / 2
            }

            Item {
                width: parent.width
                height: parent.height / 2

                ProgressBar {
                    id: progressBar
                    value: bufferProgress
                    visible: showProgressBar
                    anchors.fill: parent
                }

                PlayerControlsForm {
                    id: controls

                    visible: !showProgressBar
                    isPlaying: player.isPlaying
                    isStared: player.isFavorite
                    anchors.fill: parent
                }
            }
        }
    }

    PushUpMenu {
        id: playerMenu
        width: parent.width

        MenuLabel {
            text: qsTr("Sleep timer")
        }

        MenuLabelSmal {
            id: sleepTimerLabel
            visible: sleepTimerActive
            text: sleepTimerText
        }

        MenuItem{
            id: cancelSleepTimer
            visible: sleepTimerActive
            text: qsTr("Cancel")
        }

        MenuItem {
            id: setSleepTimer
            visible: !sleepTimerActive
            text: qsTr("Set")
        }
    }
}
