import QtQuick 2.6
import Sailfish.Silica 1.0
import "../"

Column {
    id: root

    signal clicked

    property string clockText
    property bool showText
    property alias checked: m_switch.checked
    
    Switch {
        id: m_switch
        width: parent.width
        
        icon.source: "image://theme/icon-m-timer"
        onClicked: root.clicked()
    }

    Label {
        id: timerText
        text: root.clockText
        color: Theme.highlightColor
        font.pixelSize: Theme.fontSizeExtraSmall
        
        anchors.horizontalCenter: m_switch.horizontalCenter
        visible: root.showText
    }
}
