import QtQuick 2.6
import Sailfish.Silica 1.0
import it.kempe.AudioPlayer 1.0
import "../sleeptimer"
import "../js/Favorites.js" as FavoritesUtils

DockedAudioPlayerSmallForm {
    id: player

    // Property assignment
    isPlaying: AudioPlayer.isPlaying
    isFavorite: FavoritesUtils.isFavorite(window.stationData);
    currentTrack: AudioPlayer.title
    bufferProgress: AudioPlayer.bufferProgress
    showProgressBar: AudioPlayer.isLoading || AudioPlayer.isBuffering
    sleepTimerActive: sleepTimer.running
    sleepTimerText: sleepTimer.clockText


    // Triggers
    opener.onClicked: console.debug("Show fullscreen player controls")
    buttonPlay.onClicked: AudioPlayer.togglePlayback();
    timerSwitch.onClicked: toggleSleepTimer()
    favoriteSwitch.onClicked: player.isFavorite = FavoritesUtils.toogleFavorite(window.stationData)


    // Components
    SleepTimer {
        id: sleepTimer
        onSleepTriggered: {
            timerSwitch.checked = false
            AudioPlayer.stop()
        }
    }

    Component {
        id: sleepTimerDialog
        SleepTimerDialog {
            onAbort: timerSwitch.checked = false
            onActivateTimer: sleepTimer.startTimer(sec)
        }
    }

    function toggleSleepTimer() {
        playerMenu.close()
        if (timerSwitch.checked) {
            pageStack.push(sleepTimerDialog)
        } else {
            sleepTimer.stopTimer()
        }
    }
}
