import QtQuick 2.6
import Sailfish.Silica 1.0
import it.kempe.AudioPlayer 1.0
import "../sleeptimer"
import "../js/Favorites.js" as FavoritesUtils

DockedAudioPlayerForm {
    id: player

    // Property assignment
    isPlaying: AudioPlayer.isPlaying
    isFavorite: FavoritesUtils.isFavorite(window.stationData);
    currentTrack: AudioPlayer.title
    bufferProgress: AudioPlayer.bufferProgress
    showProgressBar: AudioPlayer.isLoading || AudioPlayer.isBuffering
    sleepTimerActive: sleepTimer.running
    sleepTimerText: sleepTimer.clockText


    // Triggers
    opener.onClicked: console.debug("Show fullscreen player controls")
    buttonPlay.onClicked: AudioPlayer.togglePlayback();
    buttonStar.onClicked: player.isFavorite = FavoritesUtils.toogleFavorite(window.stationData)
    buttonPrevious.onClicked: playPrev()
    buttonNext.onClicked: playNext()
    cancelSleepTimer.onClicked: sleepTimer.stopTimer()
    setSleepTimer.onClicked: {
        playerMenu.close()
        pageStack.push(sleepTimerDialog)
    }


    // Components
    SleepTimer {
        id: sleepTimer
        onSleepTriggered: {
            AudioPlayer.stop()
        }
    }

    Component {
        id: sleepTimerDialog
        SleepTimerDialog {
            onActivateTimer: sleepTimer.startTimer(sec)
        }
    }
}
