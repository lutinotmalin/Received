import QtQuick 2.6
import Sailfish.Silica 1.0
import it.kempe.AudioPlayer 1.0
import "../js/Favorites.js" as FavoritesUtils

DockedPanel {
    id: dockedAudioPlayerLoader

    property Item player

    // Layout
    width: parent.width
    dock: Dock.Bottom
    Behavior on opacity { FadeAnimation { duration: 250 } }
    Behavior on height { NumberAnimation { duration: 250 } }
    Behavior on y { }

    contentHeight: height
    flickableDirection: Flickable.VerticalFlick

    Loader {
        id: dockedAudioPlayer
        width: parent.width

        onLoaded: {
            dockedAudioPlayerLoader.player = dockedAudioPlayer.item
            dockedAudioPlayerLoader.height = dockedAudioPlayer.item.height
        }
    }

    Component.onCompleted: {
        updateDockedAudioPlayerSource()
    }

    function updateDockedAudioPlayerSource() {
        dockedAudioPlayer.source = Qt.resolvedUrl(settings.value("playerLayout") + ".qml")
    }

    // Helper functions
    function play(station) {
        if (!expanded)
            open = true

        window.stationData = JSON.parse(JSON.stringify(station));
        AudioPlayer.loadUrl(window.stationData.url)
    }

    function playNext() {
        var nextStation = FavoritesUtils.getNextFavorite(window.stationData);
        console.debug("Playing next favorite", JSON.stringify(nextStation));
        play(nextStation);
    }

    function playPrev() {
        var prevStation = FavoritesUtils.getPrevFavorite(window.stationData);
        console.debug("Playing prev favorite", JSON.stringify(prevStation));
        play(prevStation);
    }
}
