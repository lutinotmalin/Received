# -*- coding: utf-8 -*-

import pyotherside
import requests

USER_AGENT = "XBMC not really, Recived on sailfish"

recomendedURL = "info/broadcast/editorialreccomendationsembedded"
top100URL = "info/menu/broadcastsofcategory"
mostWantedURL = "info/account/getmostwantedbroadcastlists"
searchURL = "info/index/searchembeddedbroadcast"
categoriesURL = "info/menu/valuesofcategory"
stationsByCategoriesURL = "info/menu/broadcastsofcategory"
stationByIdURL = "info/broadcast/getbroadcastembedded"

class Radio_API:
    def __init__(self):
        self.apiBaseUrl = "http://www.rad.io"
        pass

    def setApiBaseUrl(self, apiUrl):
        self.apiBaseUrl = apiUrl
        pyotherside.send('log', "setting apiBaseUrl: {}".format(apiUrl))
        return True


    def getTopStations(self):
        params = {'category': '_top'}
        response = self.doRequest(top100URL, params)
        return response

    def getRecomendedStations(self):
        response = self.doRequest(recomendedURL)
        return response

    def getLocalStations(self):
        response = self.getMostWantedStations()
        return response['localBroadcasts']

    def getMostWantedStations(self):
        # Available lists from most wanted
        #   topBroadcasts, recommendedBroadcasts, localBroadcasts
        # Lists that could be used if impementing login
        #   historyBroadcasts, bookmarkedBroadcasts
        params = {'sizeoflists': '100'}
        return self.doRequest(mostWantedURL, params)


    def getCategories(self, category):
        # 'genre', 'topic', 'country', 'city', 'language'
        params = {"category": "_{0}".format(category)}
        pyotherside.send('log', "getCategories for: {}".format(params))
        response = self.doRequest(categoriesURL, params)
        return response

    def getStationsByCategory(self, category, value):
        params = {"category": "_{0}".format(category), "value": value}
        pyotherside.send('log', "getStationsByCategory params: {0}".format(params))
        response = self.doRequest(stationsByCategoriesURL, params)
        return response

    def getSearchResults(self, s):
        pyotherside.send('log', "Searching for: {}".format(s))
        params = {"q": s, "start": "0", "rows": "100",
            "streamcontentformats": "aac,mp3"}
        response = self.doRequest(searchURL, params)
        return response

    def getStationById(self, id):
        pyotherside.send('log', "Getting station: {}".format(id))
        params = {'broadcast': id}
        response = self.doRequest(stationByIdURL, params)
        return response

    def doRequest(self, url, params=None):
        headers = {'user-agent': USER_AGENT}
        url = "{0}/{1}".format(self.apiBaseUrl, url)
        pyotherside.send('log', "Request: {0} {1} {2}".format(url, params, headers))

        r = requests.get(url, params=params, headers=headers)
        return r.json()

radio = Radio_API();
