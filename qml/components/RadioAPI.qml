import QtQuick 2.6
import io.thp.pyotherside 1.4
import "./js/Rad.js" as Rad
import "./js/Favorites.js" as FavoritesUtils

Python {
    id: radioAPI

    Component.onCompleted: {
        addImportPath(Qt.resolvedUrl('./python'));

        setHandler('log', function(msg) {
            console.log("Pyton log:", msg)
        });

        importModule('api', function () {
            setApiBaseUrl()
        });
    }

    function setApiBaseUrl() {
        call('api.radio.setApiBaseUrl', [settings.value("apiUrl")], function(response) {
            console.log("apiBaseUrl updated: ", response)
        });
    }

    function playStationById(id) {
        loading = true;
        call('api.radio.getStationById', [''+id], function(response) {
            var stationData = Rad.getStationFromRadioJson(response);
            console.log("Playing station:", JSON.stringify(stationData))

            player.play(stationData);
            loading = false;
        });
    }

    function addRadIoAsFavorite(id) {
        loading = true;
        call('api.radio.getStationById', [''+id], function(response) {
            var stationData = Rad.getStationFromRadioJson(response);
            FavoritesUtils.addFavorite(stationData)

            loading = false
        });
    }

    function updateFavorite(oldFavorite) {
        loading = true;
        call('api.radio.getStationById', [''+oldFavorite.radIoId], function(response) {
            var stationData = Rad.getStationFromRadioJson(response);
            FavoritesUtils.removeFavorite(oldFavorite);
            FavoritesUtils.addFavorite(stationData);
            console.log("Updated", JSON.stringify(oldFavorite));

            loading = false;
        });
    }

    function getCategories(cat, mList) {
        window.loading = true;
        call("api.radio.getCategories", [cat], function(response) {
            console.log("Number of results:", response.length)
            mList.clear()

            response.forEach(function(title) {
                mList.append({ title: title, category: cat})
            });

            loading = false
        });
    }

    function getTop100(mList) {
        getStationsAndUpdateList("api.radio.getTopStations", [], mList);
    }

    function getRecomended(mList) {
        getStationsAndUpdateList("api.radio.getRecomendedStations", [], mList);
    }

    function getLocal(mList) {
        getStationsAndUpdateList("api.radio.getLocalStations", [], mList)
    }

    function getStationByCategory(category, value, mList) {
        getStationsAndUpdateList("api.radio.getStationsByCategory", [category, value], mList)
    }

    function search(s, mList) {
        getStationsAndUpdateList("api.radio.getSearchResults", [s], mList)
    }

    function getStationsAndUpdateList(method, params, mList) {
        window.loading = true;
        call(method, params, function(response) {
            console.log("Number of results:", response.length)
            mList.clear()

            response.forEach(function(station) {
                if(station.playable === "FREE") {
                    mList.append(station)
                }
            });

            loading = false
        });
    }

    onError: {
        console.log('python error: ' + traceback);
    }

    onReceived: {
        console.log('got message from python: ' + data);
    }
}
