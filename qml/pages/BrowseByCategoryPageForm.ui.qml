import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"


Page {
    id: page

    property alias browseByListView: browseByListView
    property alias browseByModel: browseByModel
    property alias navigationMenu: navigationMenu

    property string category: ""
    property string headerTitle: ""

    SilicaListView {
        id: browseByListView
        VerticalScrollDecorator { flickable: browseByListView }
        anchors.fill: parent

        NavigationMenu {
            id: navigationMenu
            hideBrowseAction: true
        }

        header: Column {
                    PageHeader {
                        title: qsTr("Browse by") + " " + headerTitle
                        width: page.width
                    }
                }

        currentIndex: -1
        model: ListModel {
            id: browseByModel
        }

        delegate: ListItem {
            id: listItem
            width: parent.width
            contentHeight: Theme.itemSizeSmall

            Label {
                text: title
                font.bold: true
                color: Theme.primaryColor;
                anchors.fill: parent
                anchors.leftMargin: 25
                anchors.topMargin: 7
            }

            Connections {
                target: listItem
                onClicked: browseByListView.currentIndex = index
            }
        }
    }
}
