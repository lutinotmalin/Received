import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components/js/Storage.js" as DB
import "../components/js/Favorites.js" as Favorites

SettingsPageForm {

    aboutAction.onClicked: console.log("TODO")

    buttonResetDb.onClicked: {
        Remorse.popupAction(buttonResetDb, qsTr("Droping DB"), function() {
                                    DB.dropDB();
                                    Favorites.updateFavorites()});
    }

    onApiLanguageChanged: {
        settings.setValue("apiUrl", url)
        radioAPI.setApiBaseUrl()
    }

    onPlayerLayoutChanged: {
        settings.setValue("playerLayout", fileName)
        console.debug("Changing layout to", fileName)
    }

    Component.onCompleted: {
        // Select correct Api language
        for (var i = 0; i < apiLanguageModel.count; i++) {
            var item = apiLanguageModel.get(i)
            if (settings.value("apiUrl") === item.apiUrl) {
                apiLanguageComboBox.currentIndex = i;
            }
        }

        // Select correct player layout
        for (var i = 0; i < playerLayoutModel.count; i++) {
            var item = playerLayoutModel.get(i)
            if (settings.value("playerLayout") === item.fileName) {
                playerLayoutComboBox.currentIndex = i;
            }
        }

        playerLayoutChanged.connect(window.playerLayoutUpdated)
    }
}
