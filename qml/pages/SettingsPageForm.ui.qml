import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components/listmodels/"

Page {
    id: settingsPage

    signal apiLanguageChanged(string title, string url)
    signal playerLayoutChanged(string fileName)

    property alias aboutAction: aboutAction
    property alias buttonResetDb: buttonResetDb
    property alias playerLayoutComboBox: playerLayoutComboBox
    property alias playerLayoutModel: playerLayoutModel
    property alias apiLanguageComboBox: apiLanguageComboBox
    property alias apiLanguageModel: apiLanguageModel

    SilicaFlickable {
        id: settingsPageFlickable
        anchors.fill: parent
        contentHeight: settingsColumn.height
        VerticalScrollDecorator { flickable: settingsPageFlickable }

        PullDownMenu {
            MenuItem {
                id: aboutAction
                text: qsTr("About")
            }
        }
        RemorsePopup { id: remorse }

        Column {
            id: settingsColumn
            spacing: Theme.paddingSmall
            anchors {
                margins: Theme.paddingLarge
                left: parent.left
                right: parent.right
            }

            PageHeader {
                title: qsTr("Settings")
            }

            SectionHeader {
                text: qsTr("Basic Options")
            }

            ComboBox {
                id: playerLayoutComboBox
                label: qsTr("Player layout")

                menu: ContextMenu {
                    id: playerLayoutComboBoxMenu

                    Repeater {
                    model: PlayerLayoutListModel { id: playerLayoutModel }
                        MenuItem {
                            id: playerLayout
                            text: model.title

                            Connections {
                                target: playerLayout
                                onClicked: playerLayoutChanged(model.fileName)
                            }
                        }
                    }
                }
            }

            SectionHeader {
                text: qsTr("API Options")
            }

            ComboBox {
                id: apiLanguageComboBox
                label: qsTr("API Language:")

                menu: ContextMenu {
                    id: apiLanguageComboBoxMenu
                    Repeater {
                        model: ApiLanguageListModel { id: apiLanguageModel }
                        MenuItem {
                            id: apiLanguage
                            text: model.title

                            Connections {
                                target: apiLanguage
                                onClicked: apiLanguageChanged(model.title, model.apiUrl)
                            }
                        }
                    }
                }
            }

            SectionHeader {
                text: qsTr("Advanced Options")
            }

            ValueButton {
                id: buttonResetDb
                value: qsTr("Reset DB")
            }

            Text {
                width: parent.width
                font.pixelSize: Theme.fontSizeExtraSmall
                wrapMode: Text.Wrap
                color: Theme.primaryColor
                text: qsTr("<h2>Settings info:</h2>")+"<br>"+
                      qsTr("<p>All settings are saved when you make a change</p>")+"<br>"+
					  qsTr("<p><b>API Language:</b> Sets the endpoint to be used for API calls e.g. rad.io for English and radio.de for German</p>")+"<br>"+
                      qsTr("<p><b>Reset DB:</b> Removes everything in the database and gives you a clean start</p>")
            }
        }
    }
}
