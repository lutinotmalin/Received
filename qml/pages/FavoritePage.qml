import QtQuick 2.4

FavoritePageForm {
    onDone: {
        var item = station ? station : {}

        item.url = url
        item.name = name
        item.stationLogo = logo
        item.country = country
        item.genre = genre

        station = item
    }
}
