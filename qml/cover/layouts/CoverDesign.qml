import QtQuick 2.0
import Sailfish.Silica 1.0

Item {
    property alias title: titleText.text
    property alias icon: stationIcon.source

    Image {
        id: stationIcon
        opacity: 0.1

        smooth: true
        fillMode: Image.PreserveAspectFit
        cache: true

        width: parent.width
        height: width
        transform: Rotation { origin.x: 500; origin.y: 200; axis { x: 0; y: 1; z: 0 } angle: 45 }
    }

    Text {
        id: titleText
        anchors { fill: parent; margins: 0, Theme.paddingSmall}
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignBottom

        color: Theme.primaryColor
        font.pixelSize: Theme.fontSizeSmall

        maximumLineCount: 2
        elide: Text.ElideLeft
        wrapMode: Text.WordWrap
    }
}
