<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>ApiLanguageListModel</name>
    <message>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Austrian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>French</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Portuguese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spanish</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BrowseByCategoryPageForm.ui</name>
    <message>
        <source>Browse by</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BrowsePage</name>
    <message>
        <source>Local</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top 100</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recommended</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Genre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Topic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Country</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>City</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BrowsePageForm.ui</name>
    <message>
        <source>Browse</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DockedAudioPlayerForm.ui</name>
    <message>
        <source>Sleep timer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FavoritePageForm.ui</name>
    <message>
        <source>http://mystation.com/stream.mp3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>My Station</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>http://mystation.com/logo.jpg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Logo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sweden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Country</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Genre</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FavoritesListContextMenu</name>
    <message>
        <source>Remove from favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit favorite</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FavoritesPageForm.ui</name>
    <message>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>From </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add Custom</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationMenuForm.ui</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayerLayoutListModel</name>
    <message>
        <source>Original player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Small player</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Droping DB</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPageForm.ui</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Advanced Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reset DB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>API Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;h2&gt;Settings info:&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;All settings are saved when you make a change&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;&lt;b&gt;Reset DB:&lt;/b&gt; Removes everything in the database and gives you a clean start&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;&lt;b&gt;API Language:&lt;/b&gt; Sets the endpoint to be used for API calls e.g. rad.io for English and radio.de for German&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Player layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>API Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Basic Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StationsListContextMenu</name>
    <message>
        <source>Remove from favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add to favorite</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StationsPageForm.ui</name>
    <message>
        <source>From</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <source>Search station</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Top 100</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recommended</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Local</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
